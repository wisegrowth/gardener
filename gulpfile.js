/**
 * Gulpfile for gardener library
 * @author Camilo Acuña
 */

'use strict';

var babel, clean, concat, gulp, uglify;

gulp = require('gulp');
babel = require('gulp-babel');
clean = require('gulp-clean');
concat = require('gulp-concat');
uglify = require('gulp-uglify');

gulp.task('clean-dist', function () {
  return gulp
    .src('dist', {read: false})
    .pipe(clean());
});

gulp.task('compile', ['clean-dist'], function () {
  gulp
    .src('src/{,**}/*.js')
    .pipe(babel({
      presets: ['es2015']
    }))
    // .pipe(concat('gardener.js'))
    .pipe(uglify())
    .pipe(gulp.dest('dist'));
});
