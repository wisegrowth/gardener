/**
* @module Utils
* @author Camilo Acuña
*/ 

import childProcess from 'child_process';
import {DEFAULT_FROM_TYPES} from './constants';
import lodash from 'lodash';
import q from 'q';

/**
 * Generates an parsed snake case object
 * @param {Object} Object to parse
 * @return {Object} Parsed object
 * @type {function}
 * @public
 */
export function decamelize (object) {
  return lodash.reduce(object, function (obj, value, key) {
    obj[lodash.snakeCase(key)] = value;
    return obj;
  }, {});
}

/**
 * Generates an parsed camel case object
 * @param {Object} Object to parse
 * @return {Object} Parsed object
 * @type {function}
 * @public
 */
export function camelize (object) {
  return lodash.reduce(object, function (obj, value, key) {
    obj[lodash.camelCase(key)] = value;
    return obj;
  }, {});
}

/**
 * Method for capitalize a string
 * @param {String} [str] string to apply this function
 * @return {String} respective string with capitalize applies.
 * @type {function}
 * @public
 */
export function capitalizeString (str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

/**
 * Remove all function from an collection
 * @param  {Object} object collection to parse
 * @return {Object}        collection sanitized
 * @type {function}
 * @public
 */
export function sanitizeObject (object) {
  lodah.map(object, (data) => {
    return !lodash.isFunction(data);
  });
} 

/** 
 * Method for get an arduino USB port
 * @return {Promise} promise that resolve port string
 * @type {function}
 * @public
 */
export function getArduinoPort () {
  let defer = q.defer();

  childProcess.exec('ls /dev/cu.* | grep usbmodem', (err, res) => {
      if (err) {
        defer.reject(err);
      } else {
        defer.resolve(res.replace(/\n$/, ''));
      }
  });

  return defer.promise;
}

/**  
 * Internal message's packer method for prepare message to send wit primus
 * @param  {String} action action for do it
 * @param  {String} room   room name where are you now
 * @param  {String} data   possible data to send in this message
 * @return {Object}        Object that contains message ready to send
 * @type {function}
 * @private
 */
function _internalMessagesPacker (action, room, data) {
  let message;

  message = {
    action: action,
    room: room,
    from: DEFAULT_FROM_TYPES.GARDENERS
  }

  if (!lodash.isUndefined(data)) {
    message.data = data;
  }

  return message;
}

/** 
 * Object to define recives message's packer functions
 * @type {Object}
 * @public
 */
export const messagesPacker = {
  join: function (room, data) {
    return _internalMessagesPacker('join', room, data);
  },
  request: function (room, data) {
    return _internalMessagesPacker('request', room, data);
  },
  response: function (room, data) {
    return _internalMessagesPacker('response', room, data);
  },
  stream: function (room, data) {
    return _internalMessagesPacker('stream', room, data);
  } 
}

/**
 * Calculate avg of array numbers
 * @param  {Array} [values] numbers values
 * @return {Number} avg value
 * @type {function}
 * @public
 */
export function avgList (values) {
  let avg, sum;

  sum = lodash.reduce(values, (sum, value) => {
    sum += value;
    return sum;
  }, 0);
  avg = sum / values.length;

  return avg;
}
