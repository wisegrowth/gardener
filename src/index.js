/**
* @module Gardener
* @description Gardener is a highly-scalable framework for manage intelligent growing systems
* @author Camilo Acuña
* @required GardenerBase
*/

import GardenerBase from './gardener-base';

export default new GardenerBase();
