/**
 * @module Constants
 * @author Camilo Acuña
 */ 

/**
 * Object used to define generics constants used in all library
 * @type {Object}
 * @public
 */
export const GENERICS = {
	STRING_NOT_FOUND: 'not specified'
}

/**
 * Array used to define default module's types
 * @type {Object}
 * @public
 */
export const DEFAULT_MODULE_TYPES = {
  PANEL: 'panel',
  LIGHT: 'light',
  TEMPERATURE: 'temperature',
  HUMIDITY: 'humidity',
  IRRIGATION: 'irrigation'
};

/**
 * Object used to define default gadener-base params
 * @type {Object}
 * @public
 */
export const DEFAULT_GARDENERBASE_PARAMS = {
  DEBUGMODE: false,
  REPORT_INTERVAL_TIME: '*/3 * * * * *'
}

/** 
 * Object used to define default scheduler params
 * @type {Object}
 * @public
 */
export const DEFAULT_SCHEDULER_PARAMS = {
  INTERVAL_TIME_SCHEDULE: '*/2 * * * * *'
}

/** 
 * Object used to define default action types params
 * @type {Object}
 * @public
 */
export const DEFAULT_ACTION_TYPES = {
  JOIN: 'join',
  LEAVE: 'leave',
  REQUEST: 'request',
  RESPONSE: 'response'
}

/** 
 * Object used to define default from types values
 * @type {Object}
 * @public
 */
export const DEFAULT_FROM_TYPES = {
  GARDENERS: 'gardeners',
  USERS: 'users'
}
