/**
* @module Device Class
* @author Camilo Acuña
*/

import lodash from 'lodash';
import logger from './logger';
import Scheduler from './scheduler';
import q from 'q';

/**
 * Class used to define Device behavior
 * @required Scheduler
 * @required Lodash
 * @class
 */
export default class Device {
  /**
   * Contructor Device method
   * @param  {Object} [config] object that contain an information for single device
   *
   * Example with `relay` device: 
   * {
   *   "name": "example-device",
   *   "driver": "relay", 
   *   "pin": pin-number-device
   * }
   *
   * Example with `analog-sensor` device: 
   * {
   *   "name": "example-sensor",
   *   "driver": "analog-sensor", 
   *   "pin": pin-number-device,
   *   "calibration": calibration-number-value [optional]
   * }
   */
  constructor (config) {
    /* validates class methods */
    this._validates.configIsDefined(config);
    this._validates.configIsObject(config);

    /**
     * Set of sheduler
     * @type {Scheduler}
     * @private
     */
    this._scheduler;

    /**
     * Set of isOn device var
     * @type {Boolean}
     * @private
     */
    this._isOn = false;

    /* config device class */
    lodash.extend(this, config);
  }

  /**
   * TODO: Implements this method
   * Method that return a device info
   * @return {Object} device `this` object without functions and those unused values
   * @type {function}
   * @public
   */
  getInfo () {
    let deviceInfo;

    console.log(this);

    deviceInfo = {
      scheduler: this._scheduler.getInfo()
    };

    return deviceInfo;
  }

  /**
   * Method that return a `isOn` device value
   * @return {Boolean} 'isOn' value
   * @type {function}
   * @public
   */
  get isOn () {
    return this._isOn;
  }

  /**
   * Method that 'turn on' this device
   * @return {Promise} return a promise value
   * @type {function}
   * @public
   */
  powerOn () {
    let defer = q.defer();

    this.turnOn((err, val) => {
      if (err) {
        defer.reject(err);
      } else {
        this._isOn = true;
        defer.resolve();
      }
    });

    return defer.promise;
  }

  /**
   * Method that 'turn off' this device
   * @return {Promise} return a promise value
   * @type {function}
   * @public
   */
  powerOff () {
    let defer = q.defer();

    this.turnOff((err, val) => {
      if (err) {
        defer.reject(err);
      } else {
        this._isOn = false;
        defer.resolve();
      }
    });

    return defer.promise;
  }


  /**
   * Add scheluder to module
   * @param {Object} [config] params to Scheluder constructor
   * @type {function}
   * @public
   *
   * example:
   * {
   *   timeFormat: '*_/15 * * * *', [optional]
   *   state: true,                 [optional]
   * }
   */
  addScheduler (config) {
    this._validates.addScheduler(this._scheduler);

    this._scheduler = new Scheduler(config);
  }

  /**
   * Remove scheluder of module
   * @type {function}
   * @public
   */
  removeScheduler () {
    this._validates.removeScheduler(this._scheduler);

    if (!lodash.isUndefined(this._scheduler.engine)) {
      this._scheduler.engine.cancel();
    }
    this._scheduler = undefined;
  }

  /**
   * Method for return boolean value if device has scheduler
   * @return {Boolean} status of scheduler
   * @type {function}
   * @public
   */
  hasScheduler () {
    return lodash.isUndefined(this._scheduler) ? false: true;
  }

  /**
   * Method for return scheduler value
   * @return {Object} current scheduler
   * @type {function}
   * @public
   */
  get scheduler () {
    return this._scheduler;
  }

  /**
   * Return object with validates functions for Device Class
   * @return {Object} all validation hash values
   * @type {function}
   * @private
   */
  get _validates () {
    return {
      configIsDefined: (config) => {
        if (lodash.isUndefined(config)) {throw new Error('config param is not defined');}
      },
      configIsObject: (config) => {
        if (!lodash.isObject(config)) {throw new Error('config param is not object');}
      },
      removeScheduler: (scheduler) => {
        if (lodash.isUndefined(scheduler)) {throw new Error(`Device not has a scheduler`)};
      },
      addScheduler: (scheduler) => {
        if (!lodash.isUndefined(scheduler)) {throw new Error(`Device has already a scheluder`)};
      }
    }
  }
}
