/**
* @module Scheduler Class
* @author Camilo Acuña
*/

import ee2 from 'eventemitter2';
import nodeSchedule from 'node-schedule';
import lodash from 'lodash';
import logger from './logger';
import q from 'q';
import {GENERICS, DEFAULT_SCHEDULER_PARAMS} from './constants';

/**
 * Class used to define Scheduler behavior
 * @event [ring] when calls a scheduleJob function based in _timeFormat value
 * @requires NodeSchedule
 * @requires EventEmitter2
 * @class
 */
export default class Scheduler extends ee2.EventEmitter2 {
  /**
   * Default contructor method for Scheduler class
   * @param  {Object} config object information values
   *
   * Example:
   * { 
   *   timeFormat: '*_/15 * * * *', [optional]
   *   state: true                  [optional]
   * }
   */
  constructor (config = {}) {
    super({
      wildcard: true,
      delimiter: '::', 
      newListener: false, 
      maxListeners: 20
    });

    /**
     * Set a scheduler flag
     * @type {Boolean}
     * @private
     */
    this._isScheduled = false;

    /**
     * Define a name for scheduler
     * @type {String}
     * @private
     */
    this._name = config.name || GENERICS.STRING_NOT_FOUND;

    /**
     * Define a time's format
     * @type {String}
     * @private
     */
     this._timeFormat = config.timeFormat || DEFAULT_SCHEDULER_PARAMS.INTERVAL_TIME_SCHEDULE;

    /**
     * Engine of scheduler based in NodeSchedule dependency
     * @type {Job}
     * @private
     */
    this._engine;


    /* if exist stat*/
    if (!lodash.isUndefined(config.state) && !config.state) {
      // TODO: remplace this if condition for correct statement
    } else {
      this.start();
    }
  }

  /**
   * Method for stop a scheduler
   * @type {function}
   * @public
   */
  stop () {
    this._validates.stop(this._isScheduled);
    
    this._engine.cancel();
    this._isScheduled = false;
  }

  /**
   * Method for start a schedule
   * @event [ring] when calls a scheduleJob function based in _timeFormat value
   * @return {Promise} promise instance
   * @type {function}
   * @public
   */
  start () {
    this._validates.start(this._isScheduled);

    this._engine = nodeSchedule.scheduleJob(this._timeFormat, () => {
      this.emit('ring');
    });

    this._isScheduled = true;
  }

  /**
   * Method for pause/reanude scheluder process
   * @type {function}
   * @public
   */
  toggle () {
    if (this._isScheduled) {
      this.stop();
    } else {
      this.start();
    }
  }

  /**
   * Method that return a boolean value if scheduler is starteted or not
   * @return {Boolean} isScheluded bool value
   * @type {function}
   * @public
   */
  get isScheduled () {
    return this._isScheduled;
  }

  /**
   * Method that return all information of scheduler without engine value
   * @type {function}
   * @public
   */
  getInfo () {
    let schedulerInfo = {
      isScheduled: this._isScheduled,
      timeFormat: this._timeFormat
    };
    return schedulerInfo;
  }

  /**
   * Method that return engine scheduler
   * @type {function}
   * @public
   */
  get engine () {
    return this._engine;
  }

  /**
   * Return object with validates functions for Scheduler Class
   * @return {Object} all validation hash values
   * @type {function}
   * @private
   */
  get _validates () {
    return {
      stop: (isScheduled) => {
        if (!isScheduled) {throw new Error(`The scheduler is already stoped`)};
      },
      start: (isScheduled) => {
        if (isScheduled) {throw new Error(`The scheduler is already started`)};
      }
    }
  }
}
