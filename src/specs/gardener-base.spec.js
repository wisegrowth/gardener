/**
* GardenerBase Specs
* @author Camilo Acuña
*/

let expect;

import chai from 'chai';
import dotenv from 'dotenv';
import fs from 'fs';
import GardenerBase from '../gardener-base';

expect = chai.expect;

if (fs.existsSync('.env')) {
  dotenv.load();
}

describe('GardenerBase Class', () => {
  let example, gardener;

  example = { 
    "name": "acef8be3-2c77-4f0a-bf28-519a11358cf2",
    "debugMode": false,
    "connections": {
      "arduino": { 
        "name": "arduino",
        "adaptor": "firmata", 
        "port": "/dev/cu.usbmodem1411"
      }
    },
    "modules": {
      "light": {
        "name": "light",
        "devices": {
          "bulb": { 
            "name": "bulb",
            "driver": "relay", 
            "pin": 13 
          }
        }
      }
    }
  };

  beforeEach(() => {
    gardener = new GardenerBase();
  });

  it('should return modules object', () => {
    expect(gardener.modules).to.be.an('object');
    expect(gardener.modules).to.be.empty;
  });

  it('should return socket object', () => {
    expect(gardener.socket).to.be.undefined;
    gardener.connect();
    expect(gardener.socket).to.be.an('object');
    expect(gardener.socket).to.not.be.undefined;
  });

  it('should return object from getInfo()', () => {
    let getInfoKeys = ['debugMode', 'isConfigured', 'isRunning' , 'isConnected'];
    expect(gardener.getInfo()).to.not.be.undefined;
    expect(gardener.getInfo()).to.be.an('object');
    expect(gardener.getInfo()).to.have.all.keys(getInfoKeys);
  });

  xit('should add new module to gardener instance', () => {});
  xit('should remove a specific module of gardener instance', () => {});

  it('should connect to big brother', () => {
    expect(gardener.socket).to.be.undefined;
    expect(gardener.getInfo().isConnected).to.be.false;
    gardener.connect();
    expect(gardener.getInfo().isConnected).to.be.true;
    expect(gardener.socket).to.not.be.undefined;
    expect(gardener.socket).to.be.an('object');
    expect(() => {gardener.connect()}).to.throw('The greenhouse is connected to big brother');
  });

  it('should show throw when isConnect gardener var is true', () => {
    gardener.connect();
    expect(() => {gardener.connect()}).to.throw('The greenhouse is connected to big brother');
  });

  it('should disconnect to big brother', () => {
    gardener.connect();
    expect(gardener.getInfo().isConnected).to.be.true;
    gardener.disconnect();
    expect(gardener.getInfo().isConnected).to.be.false;
  });

  it('should configure a robot in gardener instance', () => {
    expect(gardener.getInfo().isConfigured).to.be.false;
    expect(gardener.getInfo().isRunning).to.be.false;
    gardener.config(example);
    expect(gardener.getInfo().isConfigured).to.be.true;
    expect(gardener.getInfo().isRunning).to.be.false;
  });

  /* change from xit to it for realize this test */
  xit('should run a robot in gardener instance', (done) => {
    expect(gardener.getInfo().isConfigured).to.be.false;
    expect(gardener.getInfo().isRunning).to.be.false;
    gardener.config(example);
    expect(gardener.getInfo().isConfigured).to.be.true;
    expect(gardener.getInfo().isRunning).to.be.false;
    gardener.run().then(() => {
      expect(gardener.getInfo().isConfigured).to.be.true;
      expect(gardener.getInfo().isRunning).to.be.true;
      done();
    });
  });

  /* change from xit to it for realize this test */
  xit('should stop a robot in gardener instance', () => {
    gardener.config(example);
    gardener.run().then(() => {
      expect(gardener.getInfo().isConfigured).to.be.true;
      expect(gardener.getInfo().isRunning).to.be.true;
      gardener.stop();
      expect(gardener.getInfo().isConfigured).to.be.true;
      expect(gardener.getInfo().isRunning).to.be.false;
      done();
    });
  });
});
