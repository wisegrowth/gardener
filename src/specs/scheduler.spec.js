/**
* Scheduler Specs
* @author Camilo Acuña
*/

let expect;

import chai from 'chai';
import dotenv from 'dotenv';
import fs from 'fs';
import Scheduler from '../scheduler';

expect = chai.expect;

if (fs.existsSync('.env')) {
  dotenv.load();
}

describe('Scheduler Class', () => {
  let scheduler;

  it('should contruct Scheduler class with false state param value', () => {
    scheduler = new Scheduler({state: false});
    expect(scheduler.isScheduled).to.be.false;
  });

  it('should contruct Scheduler class with true state param value', () => {
    scheduler = new Scheduler({state: true});
    expect(scheduler.isScheduled).to.be.true;
  });

  it('should start a scheduler instance', () => {
    scheduler = new Scheduler({state: false});
    expect(scheduler.isScheduled).to.be.false;
    scheduler.start();
    expect(scheduler.isScheduled).to.be.true;
  });
  
  it('should stop a scheduler instance', () => {
    scheduler = new Scheduler({state: true});
    expect(scheduler.isScheduled).to.be.true;
    scheduler.stop();
    expect(scheduler.isScheduled).to.be.false;
  });
  
  it('should toggle a scheduler instance', () => {
    scheduler = new Scheduler();
    expect(scheduler.isScheduled).to.be.true;
    scheduler.toggle();
    expect(scheduler.isScheduled).to.be.false;    
  });
  
  it('should return isScheluded boolean', () => {
    scheduler = new Scheduler();
    expect(scheduler.isScheduled).to.be.an('boolean');
  });

  it('should return getInfo object', () => {
    let keys = ['isScheduled', 'timeFormat'];
    scheduler = new Scheduler();

    expect(scheduler.getInfo()).to.be.an('object');
    expect(scheduler.getInfo()).to.have.all.keys(keys);
  });
});
