/**
* Constants Specs
* @author Camilo Acuña
*/

let expect;

import chai from 'chai';
import dotenv from 'dotenv';
import fs from 'fs';
import {GENERICS, DEFAULT_GARDENERBASE_PARAMS, DEFAULT_MODULE_TYPES, DEFAULT_SCHELUDER_TYPES} from '../constants';

expect = chai.expect;

if (fs.existsSync('.env')) {
  dotenv.load();
}

describe('Constants File', () => {
  let defaultGardenerBaseParamsConstants, defaultModuleTypesConstants, defaultScheludeTypesConstants, genericsConstants;

  beforeEach(() => {
    genericsConstants = GENERICS;
    defaultGardenerBaseParamsConstants = DEFAULT_GARDENERBASE_PARAMS;
    defaultModuleTypesConstants = DEFAULT_MODULE_TYPES;
    defaultScheludeTypesConstants = DEFAULT_SCHELUDER_TYPES;
  });

 it('should return GENERICS object values', () => {
    let keys = ['STRING_NOT_FOUND'];
    expect(genericsConstants).to.not.be.undefined;
    expect(genericsConstants).to.be.an('object');
    expect(genericsConstants).to.have.all.keys(keys);
    expect(Object.keys(genericsConstants).length).to.equal(1);
  });

  it('should return DEFAULT_GARDENERBASE_PARAMS object values', () => {
    let options = ['DEBUGMODE', 'REPORT_INTERVAL_TIME'];
    expect(defaultGardenerBaseParamsConstants).to.not.be.undefined;
    expect(defaultGardenerBaseParamsConstants).to.be.an('object');
    expect(defaultGardenerBaseParamsConstants).to.have.all.keys(options);
    expect(Object.keys(defaultGardenerBaseParamsConstants).length).to.equal(2);
  });

  it('should return DEFAULT_MODULE_TYPES object values', () => {
    let values = ['LIGHT', 'TEMPERATURE', 'TEMPERATURE', 'HUMIDITY', 'IRRIGATION'];
    expect(defaultModuleTypesConstants).to.not.be.undefined;
    expect(defaultModuleTypesConstants).to.be.an('object');
    expect(defaultModuleTypesConstants).to.include.keys(values);
    expect(Object.keys(defaultModuleTypesConstants).length).to.equal(5);
  });

  // TODO: implements test for: DEFAULT_SCHEDULER_PARAMS, DEFAULT_COLORS,
  //       DEFAULT_PANELMODULE_PARAMS, DEFAULT_ACTION_TYPES, DEFAULT_FROM_TYPES
});
