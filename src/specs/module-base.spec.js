/**
* Base Module Specs
* @author Camilo Acuña
*/

let expect;

import chai from 'chai';
import dotenv from 'dotenv';
import fs from 'fs';
import GardenerBase from '../gardener-base';
import BaseModule from '../modules/base';
import Device from '../device';
import lodash from 'lodash';

expect = chai.expect;

if (fs.existsSync('.env')) {
  dotenv.load();
}

describe('BaseModule Class', () => {
  let example, gardener, baseModule;

  example = { 
    "name": "acef8be3-2c77-4f0a-bf28-519a11358cf2",
    "debugMode": false,
    "connections": {
      "arduino": { 
        "name": "arduino",
        "adaptor": "firmata", 
        "port": "/dev/cu.usbmodem1411"
      }
    },
    "modules": {
      "light": {
        "name": "light",
        "devices": {
          "bulb": { 
            "name": "bulb",
            "driver": "relay", 
            "pin": 13 
          }
        }
      }
    }
  }; 

  beforeEach(() => {
    gardener = new GardenerBase();
    gardener.config(example);
    baseModule = new BaseModule(example.modules['light'], gardener.gardenerRobot);
  });

  it('should return all devices for a respective module', () => {
    expect(baseModule.devices).to.be.defined;
    expect(baseModule.devices).to.be.an('object');
    expect(baseModule.devices).to.have.keys('bulb');
  });

  it('should call _config method in baseModule class', () => {
    expect(baseModule.devices).to.be.defined;
    expect(baseModule.devices).to.be.an('object');
    lodash.forEach(baseModule.devices, device => {
      expect(device).to.be.an.instanceof(Device);
    });
  });

  xit('turnOn()', () => {});
  xit('turnOff() ', () => {});
});
