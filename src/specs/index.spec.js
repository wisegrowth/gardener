/**
* Index Specs
* @author Camilo Acuña
*/

let expect;

import chai from 'chai';
import dotenv from 'dotenv';
import fs from 'fs';
import indexGardener from '..';
import GardenerBase from '../gardener-base';

expect = chai.expect;

if (fs.existsSync('.env')) {
  dotenv.load();
}

describe('Index File', () => {
  let gardener;

  beforeEach(() => {
    gardener = indexGardener;
  });

  it('should confirm instanceOf inside main gardener library file', () => {
    expect(gardener).to.not.be.undefined;
    expect(gardener).to.be.an.instanceof(GardenerBase);
  });
});
