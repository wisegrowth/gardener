/**
* Device Specs
* @author Camilo Acuña
*/

let expect;

import chai from 'chai';
import dotenv from 'dotenv';
import fs from 'fs';
import Device from '../device';
import Scheduler from '../scheduler';
import {GENERICS, DEFAULT_GARDENERBASE_PARAMS} from '../constants';

expect = chai.expect;

if (fs.existsSync('.env')) {
  dotenv.load();
}

describe('Device Class', () => {
  let example, device, schedulerConfig;

  example = { 
    "name": "acef8be3-2c77-4f0a-bf28-519a11358cf2",
    "debugMode": false,
    "connections": {
      "arduino": { 
        "name": "arduino",
        "adaptor": "firmata", 
        "port": "/dev/cu.usbmodem1411"
      }
    },
    "modules": {
      "light": {
        "name": "light",
        "devices": {
          "bulb": { 
            "name": "bulb",
            "driver": "relay", 
            "pin": 13 
          }
        }
      }
    }
  };

  schedulerConfig = {
    moduleName: GENERICS.STRING_NOT_FOUND,
    timeFormat: DEFAULT_GARDENERBASE_PARAMS.INTERVAL_TIME_SCHEDULE
  };

  beforeEach(() => {
    // TODO: comprobate if example values its works for all tests
    device = new Device(example.modules['light'].devices['bulb']);
  });

  it('should test hasScheduler method', () => {
    expect(device.hasScheduler()).to.be.false;
    device.addScheduler(schedulerConfig, false);
    expect(device.hasScheduler()).to.be.true;
  });

  it('should return a scheduler object', () => {
    device.addScheduler(schedulerConfig, false);
    expect(device.scheduler).to.be.defined;
    expect(device.scheduler).to.be.an.instanceof(Scheduler);
  });

  it('should adds new scheduler instance to current device', () => {
    expect(device.scheduler).to.be.undefined;
    device.addScheduler(schedulerConfig, false);
    expect(device.scheduler).to.be.defined;
    expect(device.scheduler).to.be.an.instanceof(Scheduler);
  });

  it('should remove current scheduler to device', () => {
    device.addScheduler(schedulerConfig, false);
    device.removeScheduler();
    expect(device.scheduler).to.be.undefined;
    expect(device.scheduler).to.be.not.an.instanceof(Scheduler);
  });
});
