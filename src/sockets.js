/**
* @module Sokets
* @author Camilo Acuña
*/

import q from 'q';
import lodash from 'lodash';
import logger from './logger';

/**
* void function for validate socket connection
* @param {Object} io socket connection instance
* @Function
*/
function validateConnection (socket) {
  let gardenerToken, from, socketId;

  from = socket.handshake.query.from;
  gardenerToken = socket.handshake.query.gardenerToken;
  socketId = socket.id;
 
  // Do not suscribe a gardener/user unless it provides a gardenerToken
  if (lodash.isUndefined(gardenerToken)) {
    logger('[Connection] rejected by not having `GARDENER_TOKEN` value');
    return false;
  }

  // If gardenerToken is no valid 
  if (gardenerToken !== process.env.GARDENER_TOKEN) {
    logger('[Connection] rejected by not correct `GARDENER_TOKEN` value');
    return false;
  }

  return true;
}

/**
* void function for initialice all sockete emits and listener functions
* @param {Object} io socket connection instance
* @param {Object} gardener gardener instance
* @Function
*/
export default function socketConfig (io, gardener) {
  let defer = q.defer();

  /**
   * Event triggered when a new user is connected reads a session
   * token wich is sent by the user via it's handshake query object
   */
  io.on('connection', socket => {
    if (validateConnection(socket)) {
      logger('[Connection] `Central API` has been connected');

      /**
       *  TODO: Implements `disconnect` socket listener method
       */
      socket.on('disconnect', () => {});
      
      // resolve promise with socket value
      defer.resolve(socket);
    } else {
      logger('[Error] in validate socket connection');
      throw new Error('[Error] in validate socket connection');
    }
  });

  return defer.promise;
}
