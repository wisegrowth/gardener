/**
* @module Light Events Module
* @author Camilo Acuña
*/

import lodash from 'lodash';
import {messagesPacker} from '../../utils';

/**
* void function for define light module listenner/emitters events
* @param {GardenerBase} gardener gardener instance
* @type {function}
* @public
*/
export default function lightModuleEvents (gardener) {
  const ACTIONS = {
    ON: 'on',
    OFF: 'off',
    GET_INTENSITY: 'get-intensity'
  };

  lodash.forEach(ACTIONS, action => {
    let msg = messagesPacker.join(`module:light:${action}`);
    gardener.socket.write(msg);
  });

  gardener.socket.on('data', message => {
    let module = gardener.modules['light'];
    /** 
     * If condition for listen only `request` actions
     */
    if (message.action === 'request') {
      /** 
       * If condition for 'on' action
       */
      if (message.room === `module:light:${ACTIONS.ON}`) {
        module.on()
          .then(() => {
            let msg = messagesPacker.response(message.room);
            gardener.socket.write(msg);
          });
      }
      /** 
       * If condition for 'off' action
       */
      if (message.room === `module:light:${ACTIONS.OFF}`) {
        module.off()
          .then(() => {
            let msg = messagesPacker.response(message.room);
            gardener.socket.write(msg);
          });
      }
      /** 
       * If condition for 'get-intensity' action
       */
      if (message.room === `module:light:${ACTIONS.GET_INTENSITY}`) {
        module.getIntensity()
          .then(intensity => {
            let msg = messagesPacker.response(message.room, {intensity: intensity});
            gardener.socket.write(msg);
          });
      }
    }
  });
}
