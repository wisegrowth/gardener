/**
 * @module Light Module
 * @author Camilo Acuña
 */

import q from 'q';
import lodash from 'lodash';
import BaseModule from '../base';
import {avgList} from '../../utils';
import lightModuleEvents from './light.events'
import {DEFAULT_MODELS_TYPES} from './light.constants';
import {LIGHT_MODULE_SENSOR_PARSERS} from './light.parsers'

/**
 * Light Module Class
 * @required BaseModule
 * @required lodash
 * @required q
 * @class
 */
export default class LightModule extends BaseModule {
  /**
   * LightModule Class constructor method
   * @param  {Object} moduleConfig  Module object params
   * @param  {GardenerBase} gardener BaseGardener instance
   */
  constructor (moduleConfig, gardener) {
    function listen () {
      lightModuleEvents(gardener);
    }
    
    super(moduleConfig, gardener.gardenerRobot);

    if (gardener.isConnected) {
      listen();
    } else {
      gardener.on('connected', () => {
        listen();
      });
    }

    this._showInReport = true;
  }

  /**
   * Method that return humidity module value
   * @return {Promise} promise with intensity value
   * @type {function}
   * @public
   */
  getIntensity () {
    let defer, promises;

    defer = q.defer();
    promises = [];

    lodash.forEach(this._devices, device => {
      if (device.sensorType === 'intensity') {
        switch (device.model) {
          case DEFAULT_MODELS_TYPES.TEMT6000:
            let parser = LIGHT_MODULE_SENSOR_PARSERS.TEMT6000;
            promises.push(this.getDeviceValue(device, parser));
            break;
          
          // TODO: implements a valid sensor method with `throw error
        }
      }
    });

    q.all(promises)
      .then(intensities => {
        let avg = avgList(intensities);
        defer.resolve(avg);
      }, err => {
        defer.reject();
      });

    return defer.promise;
  }

  /** 
   * Method that return intensity module info
   * @return {Promise} promise when is resolved handing info object values
   * @type {function}
   * @public
   */
  getInfo () {
    let defer, intensityInfo;

    defer = q.defer();
    intensityInfo = super._getInfo();

    this.getIntensity()
      .then(intensity => {
        intensityInfo.intensity = intensity;

        defer.resolve(intensityInfo);
      });

    return defer.promise;
  }
}
