/**
 * @module Constants for light module
 * @author Camilo Acuña
 */ 

/**
 * Object used to define light model sensor types
 * @type {Object}
 * @public
 */
export const DEFAULT_MODELS_TYPES = {
  TEMT6000: 'TEMT6000'
}
