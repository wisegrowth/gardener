/**
* @module Light Parsers Module
* @author Camilo Acuña
*/

import q from 'q';

/**
 * Parser for TEMT6000 light sensor 
 * @see http://bildr.org/2011/07/ds18b20-arduino/
 * @param  {Number} [value] Sensor read value
 * @return {Number} parsed value
 */
function TEMT6000 (value) {
  let percentage = (100 / 1024.0) * value;

  return percentage;
}

/**
* Object to define light module sensor parsers
* @type {object}
* @public
*/
export const LIGHT_MODULE_SENSOR_PARSERS = {
  TEMT6000: TEMT6000
}
