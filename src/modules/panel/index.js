/**
 * @module Panel Module
 * @author Camilo Acuña
 */

import q from 'q';
import lodash from 'lodash'
import BaseModule from '../base';
import {capitalizeString} from '../../utils';
import panelModuleEvents from './panel.events'
import {DEFAULT_COLORS, DEFAULT_PANELMODULE_PARAMS} from './panel.constants';

/**
 * Panel Module Class
 * @required BaseModule
 * @required lodash
 * @required q
 * @class
 */
export default class PanelModule extends BaseModule {
  /**
   * PanelModule Class constructor method
   * @param  {Object} moduleConfig  Module object params
   * @param  {Object} gardener BaseGardener instance
   */
  constructor (moduleConfig, gardener) {
    function listen () {
      panelModuleEvents(gardener);
    }
    
    super(moduleConfig, gardener.gardenerRobot);
    
    if (gardener.isConnected) {
      listen();
    } else {
      gardener.on('connected', () => {
        listen();
      });
    }

    gardener.emit('add-module:panel', this);
  }

  /**
   * Method for config a led colors when gardener emits an event
   * @return {Promise} Promise instance
   * @type {function}
   * @public
   */
  config (gardener) {
    let defer, ledConfigs, promises;

    defer = q.defer();
    promises = [];

    ledConfigs = [{
      name: 'first'
    } ,{
      name: 'second'
    }];

    ledConfigs[0].color = gardener.isConnected ? 'blue' : 'red';
    ledConfigs[1].color = gardener.isRunning ? 'green' : 'red';

    lodash.forEach(ledConfigs, ledConfig => {
      promises.push(this.setLedColor(ledConfig));
    });

    q.all(promises)
      .then(colors => {
        defer.resolve(colors);
      }, err => {
        defer.reject(err);
      });

    return defer.promise;
  }

  /**
    * Method for sets led color
    * @param {Object} opts contains name and color atributtes
    * @return {Promise} Promise instance
    * @type {function}
    * @public
    */
  setLedColor (opts) {
    let colorValue, defer;

    defer = q.defer();

    if (lodash.isUndefined(opts.name)) {
      throw new Error('Param name is not defined.');
    } else if (lodash.isUndefined(opts.color)) {
      throw new Error('Param color is not defined.');
    }

    if (!lodash.has(DEFAULT_PANELMODULE_PARAMS, opts.name.toUpperCase())) {
      throw new Error('Invalid param name: ' + opts.name);
    }
    
    if (!lodash.has(DEFAULT_COLORS, opts.color.toUpperCase())) {
      throw new Error('Invalid param color: ' + opts.color);
    }
    colorValue =  DEFAULT_COLORS[opts.color.toUpperCase()];

    this._devices[opts.name].setRGB(colorValue, (err, value) => {
      if (err) {
        defer.reject(err);
      } else {
        defer.resolve(value);
      }
    });

    return defer.promise;
  }
}
