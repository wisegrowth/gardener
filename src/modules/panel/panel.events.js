/**
* @module Panel Events Module
* @author Camilo Acuña
*/

import lodash from 'lodash';
import {messagesPacker} from '../../utils';

/**
* void function for define panel module listenner/emitters events
* @param {GardenerBase} gardener gardener instance
* @type {function}
*/
export default function panelModuleEvents (gardener) {
  const ACTIONS = {
    SET_LED_COLOR: 'set-led-color'
  }

  lodash.forEach(ACTIONS, action => {
    gardener.socket.write({ 
      action: 'join',
      room: `module:panel:${action}`,
      from: 'gardeners'
    });
  });

  gardener.socket.on('data', (message) => {
    let module = gardener.modules['panel'];
    /** 
     * If condition for listen only `request` actions
     */
    if (message.action === 'request') {
      /** 
       * If condition for 'get' action
       */
      if (message.room === `module:panel:${ACTIONS.SET_LED_COLOR}`) {
        module.setLedColor(message.data)
          .then(color => {
            let msg = messagesPacker.response(message.room, {color: color});
            gardener.socket.write(msg);
          });
      }
    }
  });

  /* listener for gardener's event emits */
  gardener.on('started', () => {
    gardener.modules['panel'].config(gardener)
      .then(colors => {
        let msg = messagesPacker.response(message.room, colors)
        gardener.socket.write(msg);
      });
  });

  gardener.on('disconnected', () => {
    let opts = {
      name: 'first',
      color: 'red'
    }
    gardener.modules['panel'].setLedColor(opts)
      .then(color => {
        let msg = messagesPacker.response('module:panel:set-led-color', {color: color});
        gardener.socket.write(msg);
      });
  });

  gardener.on('reconnected', () => {
    let opts = {
      name: 'first',
      color: 'blue'
    }
    gardener.modules['panel'].setLedColor(opts)
      .then(color => {
        let msg = messagesPacker.response('module:panel:set-led-color', {color: color});
        gardener.socket.write(msg);
      });
  });

  gardener.on('stoped', () => {
    let opts = {
      name: 'second',
      color: 'red'
    }
    gardener.modules['panel'].setLedColor(opts)
      .then(color => {
        let msg = messagesPacker.response('module:panel:set-led-color', {color: color});
        gardener.socket.write(msg);
      });
  });

  // gardener.on('connected', () => {
  //   panelModule.success('connected');
  // });

  // gardener.on('configured', () => {
  //   panelModule.success('configured');
  // });
}
