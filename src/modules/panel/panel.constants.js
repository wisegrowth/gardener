/**
 * @module Constants for panel module
 * @author Camilo Acuña
 */ 

/**
 * Object used to define default colors params
 * @type {Object}
 * @public
 */
export const DEFAULT_COLORS = {
  GREEN: '00FF00',
  RED: 'FF0000',
  BLUE: '0000FF'
}

/**
 * Object used to define default panel module params
 * @type {Object}
 * @public
 */
export const DEFAULT_PANELMODULE_PARAMS = {
  FIRST: 'first',
  SECOND: 'second'
}
