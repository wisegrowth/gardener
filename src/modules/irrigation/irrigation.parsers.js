/**
* @module Irrigation Parsers Module
* @author Camilo Acuña
*/

import lodash from 'lodash';

/**
 * Parser for FC28 soil humidity sensor
 * @see http://www.electroschematics.com/6519/simple-soil-moisture-sensor-arduino-project/
 * @param  {Number} [value] Sensor read value
 * @return {Number} parsed value
 */
function FC28 (value) {
  return 100 - (100 / 1024) * value;
}

/**
 * Parser for sen0169 ph analog sensor 
 * @see http://dfrobot.com/wiki/index.php/Analog_pH_Meter_Pro_SKU:SEN0169
 * @param  {Array} [values] array samples values
 * @return {Number} parsed value
 */
function SEN0169 (values) {
  let phValue, slice, sort, sum;

  sort = values.sort();
  slice = sort.slice(2, 8);
  sum = lodash.reduce(slice, (sum, val) => {
    return sum + val;
  }, 0);

  phValue = sum * 5.0;
  phValue /= 1024;
  phValue /= 6;
  phValue *= 3.5;
  
  return phValue;
}

/**
* Object to define irrigation module sensor parsers
* @type {object}
* @public
*/
export const IRRIGATION_MODULE_SENSOR_PARSERS = {
  FC28: FC28,
  SEN0169: SEN0169
}
