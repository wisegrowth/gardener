/**
 * @module Constants for irrigation module
 * @author Camilo Acuña
 */ 

 /** 
  * Object used to define irrigation constants
  * @type {Object}
  * @public
  */
 export const DEFAULT_IRRIGATION_PARAMS = {
   LOWER_LIMIT: 30,
   LOWER_RELAY_NAME: 'waterpump'
 }
/**
 * Object used to define humidity model sensor types
 * @type {Object}
 * @public
 */
export const DEFAULT_MODELS_TYPES = {
  FC28: 'FC28',
  SEN0169: 'SEN0169'
}
