/**
 * @module Irrigation Module
 * @author Camilo Acuña
 */

import q from 'q';
import lodash from 'lodash';
import BaseModule from '../base';
import {avgList} from '../../utils';
import irrigationModuleEvents from './irrigation.events'
import {IRRIGATION_MODULE_SENSOR_PARSERS} from './irrigation.parsers'
import {DEFAULT_IRRIGATION_PARAMS, DEFAULT_MODELS_TYPES} from './irrigation.constants';

/**
 * Irrigation Module Class
 * @required BaseModule
 * @required lodash
 * @required q
 * @class
 */
export default class IrrigationModule extends BaseModule {
  /**
   * IrrigationModule Class constructor method
   * @param  {Object} [config]  Module object params
   * @param  {GardenerBase} [gardener] BaseGardener instance
   *
   * Example: 
   * {
   * "name": "example-module",
   * "devices": {
   *   "example-sensor": {...},
   *   "example-relay": {...},
   *   }
   * }
   */
  constructor (config, gardener) {
    function listen () {
      irrigationModuleEvents(gardener);
    }
    
    super(config, gardener.gardenerRobot);

    /** 
     * Define lower limit soil humidity value for this module
     * @type {Number}
     * @private
     */
    this._lowerLimit = config.lowerLimit || DEFAULT_IRRIGATION_PARAMS.LOWER_LIMIT;

    if (gardener.isConnected) {
      listen();
    } else {
      gardener.on('connected', () => {
        listen();
      });
    }

    this._showInReport = true;
  }

  /**
   * Method that return a soil humidity value
   * @return {Promise} promise with humidity value
   * @type {function}
   * @public
   */
  getSoil () {
    let defer, promises;

    defer = q.defer();
    promises = [];

    lodash.forEach(this._devices, device => {
      if (device.sensorType === 'soil') {
        switch (device.model) {
          case DEFAULT_MODELS_TYPES.FC28:
            let parser = IRRIGATION_MODULE_SENSOR_PARSERS.FC28;
            promises.push(this.getDeviceValue(device, parser));
            break;
          
          // TODO: implements a valid sensor method with `throw error
        }
      }
    });

    q.all(promises)
      .then(soils => {
        let avg = avgList(soils);
        defer.resolve(avg);
      }, err => {
        defer.reject(err);
      });

    return defer.promise;
  }

  /**
   * Method that return a ph water value
   * @return {Promise} promise with ph value
   * @type {function}
   * @public
   */
  getPh () {
    let defer, promises;

    defer = q.defer();
    promises = [];

    lodash.forEach(this._devices, device => {
      if (device.sensorType === 'ph') {
        switch (device.model) {
          case DEFAULT_MODELS_TYPES.SEN0169:
            let parser = IRRIGATION_MODULE_SENSOR_PARSERS.SEN0169;
            promises.push(this.getDeviceValue(device, parser, {}, 10));
            break;
          
          // TODO: implements a valid sensor method with `throw error
        }
      }
    });

    q.all(promises)
      .then(soils => {
        let avg = avgList(soils);
        defer.resolve(avg);
      }, err => {
        defer.reject(err);
      });

    return defer.promise;
  }

  /** 
   * Method that return humidity module info
   * @return {Promise} promise when is resolved handing info object values
   * @type {function}
   * @public
   */
  getInfo () {
    let defer, irrigationInfo, promises;

    defer = q.defer();
    irrigationInfo = super._getInfo();
    promises = [];

    irrigationInfo.lowerLimit = this._lowerLimit;

    promises.push(this.getSoil());
    promises.push(this.getPh());

    q.all(promises)
      .then(results => {
        irrigationInfo.soil = results[0];
        irrigationInfo.ph = results[1];
        
        defer.resolve(irrigationInfo);
      });

    return defer.promise;
  }

  /** 
   * Method for add scheduler for this module and call prescrition method when listen ring event
   * @param {Object} config object that contains scheduler values
   * @type {function}
   * @public
   *  
   * config example: 
   * { 
   *   timeFormat: '*_/15 * * * *', [optional]
   *   state: true                  [optional]
   * }
   */
  addScheduler (config) {
    let scheduler = super._addScheduler(config);
    
    scheduler.on('ring', () => {
      this._prescription();
    });
  }

  // TODO: adds removeScheduler method
  
  /** 
   * Method to excecute a devices's prescription for scheduler 
   * @type {function}
   * @private 
   */
  _prescription () {
    this.getSoil()
      .then(soil => {
        if (soil < this._lowerLimit) {
          this._devices[DEFAULT_IRRIGATION_PARAMS.LOWER_RELAY_NAME].powerOn();
        } else {
          this._devices[DEFAULT_IRRIGATION_PARAMS.LOWER_RELAY_NAME].powerOff();
        }
      });
  }

  /** 
   * Method that change a condition params of modules
   * @type {function}
   * @public
   */
  changeConditionParams (data) {
    switch (data.name) {
      case DEFAULT_IRRIGATION_PARAMS.LOWER_RELAY_NAME:
        this._lowerLimit = data.value;
        break;
    } 
  }
}
