/**
* @module Irrigation Events Module
* @author Camilo Acuña
*/

import lodash from 'lodash';
import {messagesPacker} from '../../utils';

/**
* Function to define irrigation module listenner/emitters events
* @param {GardenerBase} gardener gardener instance
* @type {function}
* @public
*/
export default function irrigationModuleEvents (gardener) {
  const ACTIONS = {
    ON: 'on',
    OFF: 'off',
    GET_SOIL: 'get-soil',
    GET_PH: 'get-ph',
    ADD_SCHEDULER: 'add-scheduler',
    TOGGLE_SCHEDULER: 'toggle-scheduler',
    CHANGE_CONDITION_PARAMS: 'change-condition-params'
  };

  lodash.forEach(ACTIONS, action => {
    let msg = messagesPacker.join(`module:irrigation:${action}`);
    gardener.socket.write(msg);
  });

  gardener.socket.on('data', message => {
    let module = gardener.modules['irrigation'];
    /** 
     * If condition for listen only `request` actions
     */
    if (message.action === 'request') {
      /** 
       * If condition for 'on' action
       */
      if (message.room === `module:irrigation:${ACTIONS.ON}`) {
        module.on()
          .then(() => {
            let msg = messagesPacker.response(message.room);
            gardener.socket.write(msg);
          });
      }
      /** 
       * If condition for 'off' action
       */
      if (message.room === `module:irrigation:${ACTIONS.OFF}`) {
        module.off()
          .then(() => {
            let msg = messagesPacker.response(message.room);
            gardener.socket.write(msg);
          });
      }
      /** 
       * If condition for 'getSoil' action
       */
      if (message.room === `module:irrigation:${ACTIONS.GET_SOIL}`) {
        module.getSoil()
          .then(soil => {
            let msg = messagesPacker.response(message.room, {soil: soil});
            gardener.socket.write(msg);
          });
      }
      /** 
       * If condition for 'getPh' action
       */
      if (message.room === `module:irrigation:${ACTIONS.GET_PH}`) {
        module.getPh()
          .then(ph => {
            let msg = messagesPacker.response(message.room, {ph: ph});
            gardener.socket.write(msg);
          });
      }
      /** 
       * If condition for 'add-scheduler' action.
       */
      if (message.room === `module:irrigation:${ACTIONS.ADD_SCHEDULER}`) {
        let data, msg;

        data = message.data;
        msg = messagesPacker.response(message.room);

        module.addScheduler(data);
        gardener.socket.write(msg);
      }
      /** 
       * If condition for 'toggle-scheduler' action.
       */
      if (message.room === `module:irrigation:${ACTIONS.TOGGLE_SCHEDULER}`) {
        let msg = messagesPacker.response(message.room);

        module.toggleScheduler();
        gardener.socket.write(msg);
      }
      /** 
       * If condition for 'change-condition-params' action.
       */
      if (message.room === `module:irrigation:${ACTIONS.CHANGE_CONDITION_PARAMS}`) {
        let data, msg;

        data = message.data;
        msg = messagesPacker.response(message.room);

        module.changeConditionParams(data);
        gardener.socket.write(msg);
      }
    }
  });
}
