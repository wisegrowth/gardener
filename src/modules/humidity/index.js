/**
 * @module Humidity Module
 * @author Camilo Acuña
 */

import q from 'q';
import lodash from 'lodash';
import BaseModule from '../base';
import {avgList} from '../../utils';
import humidityModuleEvents from './humidity.events'
import {HUMIDITY_MODULE_SENSOR_PARSERS} from './humidity.parsers';
import {DEFAULT_HUMIDITY_PARAMS, DEFAULT_MODELS_TYPES} from './humidity.constants';

/**
 * Humidity Module Class
 * @required BaseModule
 * @required lodash
 * @required q
 * @class
 */
export default class HumidityModule extends BaseModule {
  /**
   * HumidityModule Class constructor method
   * @param  {Object} [config]  Module object params
   * @param  {GardenerBase} [gardener] BaseGardener instance
   *
   * Example: 
   * {
   * "name": "example-module",
   * "devices": {
   *   "example-sensor": {...},
   *   "example-relay": {...},
   *   }
   * }
   */
  constructor (config, gardener) {
    function listen () {
      humidityModuleEvents(gardener);
    }
    
    super(config, gardener.gardenerRobot, {temperature: gardener.modules['temperature']});

    /**
     * Define upper limit humidity value for this module
     * @type {Number}
     * @private 
     */
    this._upperLimit = config.upperLimit || DEFAULT_HUMIDITY_PARAMS.UPPER_LIMIT;
    /** 
     * Define lower limit humidity value for this module
     * @type {Number}
     * @private
     */
    this._lowerLimit = config.lowerLimit || DEFAULT_HUMIDITY_PARAMS.LOWER_LIMIT;

    if (gardener.isConnected) {
      listen();
    } else {
      gardener.on('connected', () => {
        listen();
      });
    }

    this._showInReport = true;
  }

  /**
   * Method that return a humidity module value
   * @return {Promise} promise with humidity value
   * @type {function}
   * @public
   */
  get () {
    let defer, promises;

    defer = q.defer();
    promises = [];

    lodash.forEach(this._devices, device => {
      if (device.sensorType === 'humidity') {
        switch (device.model) {
          case DEFAULT_MODELS_TYPES.HIH4030:
            let parser = HUMIDITY_MODULE_SENSOR_PARSERS.HIT3040;
            promises.push(this.getDeviceValue(device, parser, this._dependencies.temperature));
            break;
          
          // TODO: implements a valid sensor method with `throw error
        }
      }
    });

    q.all(promises)
      .then(humidities => {
        let avg = avgList(humidities);
        defer.resolve(avg);
      }, (err) => {
        defer.reject(err);
      });

    return defer.promise;
  }

  /** 
   * Method for add scheduler for this module and call prescrition method when listen ring event
   * @param {Object} config object that contains scheduler values
   * @type {function}
   * @public
   *  
   * config example: 
   * { 
   *   timeFormat: '*_/15 * * * *', [optional]
   *   state: true                  [optional]
   * }
   */
  addScheduler (config) {
    let scheduler = super._addScheduler(config);
    
    scheduler.on('ring', () => {
      this._prescription();
    });
  }

  /** 
   * Method to excecute a devices's prescription for scheduler 
   * @type {function}
   * @private 
   */
  _prescription () {
    this.get()
      .then(humidity => {
        if (humidity < this._lowerLimit) {
          this._devices[DEFAULT_HUMIDITY_PARAMS.LOWER_RELAY_NAME].powerOn();
          this._devices[DEFAULT_HUMIDITY_PARAMS.UPPER_RELAY_NAME].powerOff();
        } else if (humidity > this._upperLimit) {
          this._devices[DEFAULT_HUMIDITY_PARAMS.LOWER_RELAY_NAME].powerOff();
          this._devices[DEFAULT_HUMIDITY_PARAMS.UPPER_RELAY_NAME].powerOn();
        } else {
          this._devices[DEFAULT_HUMIDITY_PARAMS.LOWER_RELAY_NAME].powerOff();
          this._devices[DEFAULT_HUMIDITY_PARAMS.UPPER_RELAY_NAME].powerOff();
        }
      });
  }

  /** 
   * Method that return humidity module info
   * @return {Promise} promise when is resolved handing info object values
   * @type {function}
   * @public
   */
  getInfo () {
    let defer, humidityInfo;

    defer = q.defer();
    humidityInfo = super._getInfo();

    humidityInfo.lowerLimit = this._lowerLimit;
    humidityInfo.upperLimit = this._upperLimit;

    this.get()
      .then(humidity => {
        humidityInfo.humidity = humidity;

        defer.resolve(humidityInfo);
      });

    return defer.promise;
  }

  /** 
   * Method that change a condition params of modules
   * @type {function}
   * @public
   */
  changeConditionParams (data) {
    switch (data.name) {
      case DEFAULT_HUMIDITY_PARAMS.UPPER_RELAY_NAME:
        this._upperLimit = data.value;
        break;

      case DEFAULT_HUMIDITY_PARAMS.LOWER_RELAY_NAME:
        this._lowerLimit = data.value;
        break;
    } 
  }
}
