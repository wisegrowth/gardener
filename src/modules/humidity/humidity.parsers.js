/**
* @module Humidity Parsers Module
* @author Camilo Acuña
*/

import q from 'q';

/**
 * Parser for HIH3040 humidity sensor 
 * @see http://bildr.org/2012/11/hih4030-arduino/
 * @required [Temperature] needs a temperature in celcius degrees
 * @param  {Number} [value] Sensor read value
 * @param  {Temperature} [temperature] temeprature module
 * @return {Number} parsed value
 */
function HIT3040 (value, temperature) {
  let defer, realHumidity, sensorHumidity, voltage;

  defer = q.defer();
  voltage = value/1023.0 * 5.0;
  sensorHumidity = 161.0 * voltage / 5.0 - 25.8;

  temperature.get()
    .then(temperature => {
      realHumidity = sensorHumidity / (1.0546 - 0.0026 * temperature); 
      defer.resolve(realHumidity);
    })

  return defer.promise;
}

/**
* Object to define HUMIDITY module sensor parsers
* @type {object}
* @public
*/
export const HUMIDITY_MODULE_SENSOR_PARSERS = {
  HIT3040: HIT3040
}
