/**
* @module Humidity Events Module
* @author Camilo Acuña
*/

import lodash from 'lodash';
import {messagesPacker} from '../../utils';

/**
* void function for define humidity module listenner/emitters events
* @param {GardenerBase} gardener gardener instance
* @type {function}
* @public
*/
export default function humidityModuleEvents (gardener) {
  const ACTIONS = {
    GET: 'get',
    ADD_SCHEDULER: 'add-scheduler',
    TOGGLE_SCHEDULER: 'toggle-scheduler',
    CHANGE_CONDITION_PARAMS: 'change-condition-params'
  };

  lodash.forEach(ACTIONS, action => {
    let msg = messagesPacker.join(`module:humidity:${action}`);
    gardener.socket.write(msg);
  });

  gardener.socket.on('data', message => {
    let module = gardener.modules['humidity'];
    /** 
     * If condition for listen only `request` actions
     */
    if (message.action === 'request') {
      /** 
       * If condition for 'get' action
       */
      if (message.room === `module:humidity:${ACTIONS.GET}`) {
        module.get()
          .then(humidity => {
            let msg = messagesPacker.response(message.room, {humidity: humidity});
            gardener.socket.write(msg);
          });
      }
      /** 
       * If condition for 'add-scheduler' action.
       */
      if (message.room === `module:humidity:${ACTIONS.ADD_SCHEDULER}`) {
        let data, msg;

        data = message.data;
        msg = messagesPacker.response(message.room);

        module.addScheduler(data);
        gardener.socket.write(msg);
      }
      /** 
       * If condition for 'toggle-scheduler' action.
       */
      if (message.room === `module:humidity:${ACTIONS.TOGGLE_SCHEDULER}`) {
        let msg = messagesPacker.response(message.room);

        module.toggleScheduler();
        gardener.socket.write(msg);
      }
      /** 
       * If condition for 'change-condition-params' action.
       */
      if (message.room === `module:humidity:${ACTIONS.CHANGE_CONDITION_PARAMS}`) {
        let data, msg;

        data = message.data;
        msg = messagesPacker.response(message.room);

        module.changeConditionParams(data);
        gardener.socket.write(msg);
      }
    }
  });
}
