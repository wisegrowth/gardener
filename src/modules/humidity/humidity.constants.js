/**
 * @module Constants for humidity module
 * @author Camilo Acuña
 */ 

 /** 
  * Object used to define humidity constants
  * @type {Object}
  * @public
  */
 export const DEFAULT_HUMIDITY_PARAMS = {
   UPPER_LIMIT: 80,
   LOWER_LIMIT: 65,
   UPPER_RELAY_NAME: 'upper',
   LOWER_RELAY_NAME: 'lower'
 }

/**
 * Object used to define humidity model sensor types
 * @type {Object}
 * @public
 */
export const DEFAULT_MODELS_TYPES = {
  HIH4030: 'HIH4030'
}
