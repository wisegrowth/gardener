/**
 * @module Base Module
 * @author Camilo Acuña
 */

import Device from '../device';
import lodash from 'lodash';
import Scheduler from '../scheduler';
import {capitalizeString} from '../utils';
import {GENERICS, DEFAULT_SCHEDULER_PARAMS} from '../constants';
import q from 'q';

/**
 * Base Module Class
 * @required Device
 * @required Scheduler
 * @required lodash
 * @required q
 * @class
 */
export default class BaseModule {
  /**
   * BaseModule class constructor method
   * @param  {Object} moduleConfig  Object that contain an module information
   * @param  {Robot} gardenerRobot [description]
   * @param  {Object} dependencies  [description] 
   *
   * ModuleConfig example:
   * {
   *   "name": "example-module",
   *   "devices": {
   *     "example-sensor": {...},
   *     "example-relay": {...}
   *   }
   * }
   *
   * Dependencies example:
   * {
   *  "module-name": {TemperatureModule}
   * }
   */
  constructor (moduleConfig, gardenerRobot, dependencies = {}) {
    /* validates class methods */
    this._validates.moduleConfigIsDefined(moduleConfig);
    this._validates.gardenerRobotIsDefined(gardenerRobot);
    this._validates.moduleConfigIsObject(moduleConfig);

    /**
     * Sets module name in this class
     * @type {String} 
     * @private
     */
    this._name = moduleConfig.name || GENERICS.STRING_NOT_FOUND;

    /**
     * Set of devices
     * @type {Object} 
     * @private
     */
    this._devices = {};

    /**
     * Dependencies method for this module
     * @type {Object} 
     * @private
     */
    this._dependencies = {};

    /** 
     * Set of schedulers they can affect
     * @type {Object} 
     * @private
     */
    this._schedulers = {};

    /**
     * Set is running boolean value
     * @type {Boolean}
     * @private
     */
    this._isRunning = false;

    /** 
     * Set  show in report value
     * @type {Boolean}
     * @private 
     */
    this._showInReport = false;

    /* config basemodule class */
    this._config(moduleConfig, gardenerRobot, dependencies);
  }

  /**
   * Method for get _devices value
   * @return {Object} devices object
   * @type {function}
   * @public
   */
  get devices () {
    return this._devices;
  }

  /**
   * Method for get name value
   * @return {String} module name
   * @type {function}
   * @public
   */
  get name () {
    return this._name;
  }

  /** 
   * Get method for showInReport value
   * @return {Boolean} showInReport value
   * @type {function}
   * @public
   */
  get showInReport () {
    return this._showInReport;
  }

  /** 
   * Method for return a base module info values
   * @return {Object} module info values
   * @type {function}
   * @private
   */
  _getInfo () {
    let baseInfo, relays;

    baseInfo = {
      name: this._name,
      isRunning: this._isRunning
    }

    relays = lodash.reduce(this._devices, (devices, device, deviceKey) => {
      if (device.driver === 'relay') {
        devices[deviceKey] = device.isOn;
      }
      return devices;
    }, {});

    lodash.assign(baseInfo, relays);

    return baseInfo;
  }

  /**
   * Method for config module and instances devices cylon robot object
   * @param {Object} [module] Object that contain module object values
   * @param {Robot} [gardenerRobot] Robot instance
   * @param {Object} [dependencies] Object that contains a module dependencies
   * @type {function}
   * @private
   *
   * Module example argument:
   * {
   * "name": "example-module",
   * "devices": {
   *   "example-sensor": {...},
   *   "example-relay": {...},
   *   }
   * }
   * 
   * Dependencies example argument:
   * {
   * "module-name": {TemperatureModule}
   * }
   */
  _config (module, gardenerRobot, dependencies) {
    lodash.forEach(module.devices, (d, deviceKey) => {
      let key, device;

      key = module.name + capitalizeString(deviceKey);
      device = lodash.assign(gardenerRobot.devices[key], module.devices[deviceKey]);

      this._devices[deviceKey] = new Device(device);
    });

    this._dependencies = lodash.assign(this._dependencies, dependencies);
  } 

  /**
   * Method for turn on light module
   * TODO: adds respective callback with event emitter for catch in other code place
   * @return {Promise} when resolved on method
   * @type {function}
   * @public
   */
  on () {
    let defer, promises;

    defer = q.defer();
    promises = [];

    lodash.forEach(this._devices, device => {
      if (device.driver === 'relay') {
        promises.push(device.powerOn());
      }
    });

    q.all(promises)
      .then(() => {
        defer.resolve();
      }, err => {
        defer.reject(err);
      })

    return defer.promise;
  }

  /**
   * Method for turn off light module
   * TODO: adds respective callback with event emitter for catch in other code place
   * @type {function}
   * @public
   */
  off () {
    let defer, promises;

    defer = q.defer();
    promises = [];

    lodash.forEach(this._devices, device => {
      if (device.driver === 'relay') {
        promises.push(device.powerOff());
      }
    });

    q.all(promises)
      .then(() => {
        defer.resolve();
      }, err => {
        defer.reject(err);
      });

    return defer.promise;
  }

  /**
   * Method for turn off a single device
   * @param  {Object} [device] device instance
   * @return {Promise}       promise instance
   * @type {function}
   * @private
   */
  _turnOff (device) {
    let defer = q.defer();

    device.turnOff()
      .then(() => {
        defer.resolve();
      });

    return defer.promise;
  }

  /** 
   * Method for add scheduler for this module
   * @param {Object} [config] object that contains scheduler values
   * @type {function}
   * @private
   * 
   * config example: 
   * { 
   *   name: 'name-example',        [optional]
   *   timeFormat: '*_/15 * * * *', [optional]
   *   state: true                  [optional]
   * }
   */
  _addScheduler (config) {
    let scheduler;

    config = config || {};

    if (!lodash.has(config, 'name')) {
      config.name = "report";
    }

    scheduler = new Scheduler(config);
    
    this._schedulers[config.name] = scheduler;
    this._isRunning = true;

    return scheduler;
  }

  /** 
   * Method for pause/reanude report scheduler
   * @return {Boolean} isScheduled boolean value
   * @type {function}
   * @public
   */
  toggleScheduler () {
    let scheduler = this._schedulers['report'];

    scheduler.toggle();
    this._isRunning = !this._isRunning;

    if (this._isRunning) {
      this.on();
    } else {
      this.off();
    }

    return scheduler.isScheduled;
  }

  /** 
   * Method for remove scheduler
   * TODO: implement this method
   * @type {function}
   * @private
   */
  _removeScheduler (id) {}

  /**
   * Method for return a device value 
   * @param  {Device}   [device] current device to read value in analog pin
   * @param  {function} [parser] respective parser method based in sensor type
   * @param  {Number}   [nSamples] number of iterations for setInterval
   * @param  {function} [time] delay time for setInterval
   * @return {Promise} promise with read value
   * @type {function}
   * @private
   * TODO: change params positions
   */
  getDeviceValue (device, parser, dependency = {}, nSamples, time = 10) {
    let count, defer, interval, samples;

    defer = q.defer();

    // TODO: Validates device param
    this._validates.getDeviceValue(parser);

    if (lodash.isUndefined(nSamples)) {
      device.analogRead((err, value) => {
        if (err) {
          defer.reject(err);
        } else {
          let parserResult = parser(value, dependency)
          defer.resolve(parserResult);
        }
      });
    } else {
      count = 0;
      samples = [];
      
      interval = setInterval(() => {
        device.analogRead((err, value) => {
          if (err) {
            defer.reject(err);
          } else {
            samples.push(value);
          }
        });

        if (++count === nSamples) {
          let parserResult = parser(samples, dependency)
          
          defer.resolve(parserResult);
          clearInterval(interval);
        }
      }, time);
    }


    return defer.promise;
  }

  /**
   * Return object with validates functions for BaseModule Class
   * @return {Object} all validation hash values
   * @type {function}
   * @private
   */
  get _validates () {
    return {
      moduleConfigIsDefined: moduleConfig => {
        if (lodash.isUndefined(moduleConfig)) {throw new Error('moduleConfig param is not defined');}
      },
      gardenerRobotIsDefined: gardenerRobot => {
        if (lodash.isUndefined(gardenerRobot)) {throw new Error('gardenerRobot param is not defined');}
      },
      moduleConfigIsObject: moduleConfig => {
        if (!lodash.isObject(moduleConfig)) {throw new Error('moduleConfig is not object');}
      },
      getDeviceValue: parser => {
        if (lodash.isUndefined(parser)) {throw new Error('parser is not defined');}
        else if (!lodash.isFunction(parser)) {throw new Error('parser is not function');}
      }
    }
  }
}
