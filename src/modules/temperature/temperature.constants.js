/**
 * @module Constants for temprature module
 * @author Camilo Acuña
 */ 

/**
 * Object used to define temperature constants
 * @type {Object}
 * @public
 */
export const DEFAULT_TEMPERATURE_PARAMS = {
  UPPER_LIMIT: 30,
  LOWER_LIMIT: 22,
  UPPER_RELAY_NAME: 'upper',
  LOWER_RELAY_NAME: 'lower'
}

/**
 * Object used to define humidity model sensor types
 * @type {Object}
 * @public
 */
export const DEFAULT_MODELS_TYPES = {
  TMP36: 'TMP36'
}
