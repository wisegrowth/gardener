/**
* @module Temperature Parsers Module
* @author Camilo Acuña
*/

import q from 'q';

/**
 * Parser for TMP36 temperature sensor 
 * @see https://learn.adafruit.com/tmp36-temperature-sensor/using-a-temp-sensor
 * @param  {Number} [value] Sensor read value
 * @return {Number} parsed value
 */
function TMP36 (value) {
  let voltage, temperature;
  
  voltage = (value * 5.0) / 1024.0;
  temperature = (voltage - 0.5) * 100;

  return temperature;
}

/**
* Object to define temperature module sensor parsers
* @type {object}
* @public
*/
export const TEMPERATURE_MODULE_SENSOR_PARSERS = {
  TMP36: TMP36
}
