/**
 * @module Temperature Module
 * @author Camilo Acuña
 */

import q from 'q';
import lodash from 'lodash';
import BaseModule from '../base';
import {avgList} from '../../utils';
import temperatureModuleEvents from './temperature.events';
import {TEMPERATURE_MODULE_SENSOR_PARSERS} from './temperature.parsers';
import {DEFAULT_TEMPERATURE_PARAMS, DEFAULT_MODELS_TYPES} from './temperature.constants';

/**
 * Temperature Module Class
 * @required BaseModule
 * @required lodash
 * @required q
 * @class
 */
export default class TemperatureModule extends BaseModule {
  /**
   * TemperatureModule Class constructor method
   * @param  {Object} config  Module object params
   * @param  {Object} gardener BaseGardener instance
   *
   * Example: 
   * {
   * "name": "example-module",
   * "devices": {
   *   "example-sensor": {...},
   *   "example-relay": {...},
   *   },
   *  upperLimit: 35, [optional]
   *  lowerLimit: 22  [optional]
   * }
   * 
   * This module used two relays type device, one for each one.
   * The name for relays are:
   *   - upper
   *   - lower
   */
  constructor (config, gardener) {
    function listen () {
      temperatureModuleEvents(gardener);
    }
    
    super(config, gardener.gardenerRobot);

    /**
     * Define upper limit temperature value for this module
     * @type {Number}
     * @private
     */
    this._upperLimit = config.upperLimit || DEFAULT_TEMPERATURE_PARAMS.UPPER_LIMIT;

    /** 
     * Define lower limit temperature value for this module
     * @type {Number}
     * @private
     */
    this._lowerLimit = config.lowerLimit || DEFAULT_TEMPERATURE_PARAMS.LOWER_LIMIT;

    if (gardener.isConnected) {
      listen();
    } else {
      gardener.on('connected', () => {
        listen();
      });
    }

    this._showInReport = true;
  }

  /**
   * Method that return a temperature module value
   * @return {Promise} promise with temperature value
   * @type {function}
   * @public
   */
  get () {
    let defer, promises;

    defer = q.defer();
    promises = [];

    lodash.forEach(this._devices, device => {
      if (device.sensorType === 'temperature') {
        switch (device.model) {
          case DEFAULT_MODELS_TYPES.TMP36:
            let parser = TEMPERATURE_MODULE_SENSOR_PARSERS.TMP36;
            promises.push(this.getDeviceValue(device, parser));
            break;
          
          // TODO: implements a valid sensor method with `throw error
        }
      }
    });

    q.all(promises)
      .then(temperatures => {
        let avg = avgList(temperatures);
        defer.resolve(avg);
      }, err => {
        defer.reject(err);
      });

    return defer.promise;
  }

  /** 
   * Method for add scheduler for this module and call prescrition 
   * method when listen ring event.
   * @param {Object} config object that contains scheduler values
   * @type {function}
   * @public
   *  
   * config example: 
   * { 
   *   name: 'example-name',        [optional]
   *   timeFormat: '*_/15 * * * *', [optional]
   *   state: true                  [optional]
   * }
   */
  addScheduler (config) {
    let scheduler = super._addScheduler(config);

    scheduler.on('ring', () => {
      this.prescription();
    });
  }

  /** 
   * Method to excecute a devices's prescription for scheduler
   * @type {function}
   * @public
   */
  prescription () {
    this.get()
      .then(temperature => {
        if (temperature < this._lowerLimit) {
          this._devices[DEFAULT_TEMPERATURE_PARAMS.LOWER_RELAY_NAME].powerOn();
          this._devices[DEFAULT_TEMPERATURE_PARAMS.UPPER_RELAY_NAME].powerOff();
        } else if (temperature > this._upperLimit) {
          this._devices[DEFAULT_TEMPERATURE_PARAMS.LOWER_RELAY_NAME].powerOff();
          this._devices[DEFAULT_TEMPERATURE_PARAMS.UPPER_RELAY_NAME].powerOn();
        } else {
          this._devices[DEFAULT_TEMPERATURE_PARAMS.LOWER_RELAY_NAME].powerOff();
          this._devices[DEFAULT_TEMPERATURE_PARAMS.UPPER_RELAY_NAME].powerOff();
        }
      });
  }

  /** 
   * Method that return temperature module info
   * @return {Promise} promise when is resolved handing info object values
   * @type {function}
   * @public
   */
  getInfo () {
    let defer, temperatureInfo;

    defer = q.defer();
    temperatureInfo = super._getInfo();

    temperatureInfo.lowerLimit = this._lowerLimit;
    temperatureInfo.upperLimit = this._upperLimit;

    this.get()
      .then(temperature => {
        temperatureInfo.temperature = temperature;

        defer.resolve(temperatureInfo);
      });

    return defer.promise;
  }

  /** 
   * Method that change a condition params of modules
   * @type {function}
   * @public
   */
  changeConditionParams (data) {
    switch (data.name) {
      case DEFAULT_TEMPERATURE_PARAMS.UPPER_RELAY_NAME:
        this._upperLimit = data.value;
        break;

      case DEFAULT_TEMPERATURE_PARAMS.LOWER_RELAY_NAME:
        this._lowerLimit = data.value;
        break;
    } 
  }

  // TODO: adds _validates method
}
