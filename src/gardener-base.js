/**
* @module GardenerBase
* @author Camilo Acuña
*/

import lodash from 'lodash';
import Cylon from 'cylon';
import ee2 from 'eventemitter2';
import gardenerBasePrimusEmitters from './gardener-emitters';
import gardenerBasePrimusListeners from './gardener-listeners';
import logger from './logger';
import q from 'q';
import {capitalizeString} from './utils';
import {DEFAULT_GARDENERBASE_PARAMS, DEFAULT_MODULE_TYPES, GENERICS} from './constants';
import HumidityModule from './modules/humidity';
import LightModule from './modules/light';
import PanelModule from './modules/panel';
import TemperatureModule from './modules/temperature';
import IrrigationModule from './modules/irrigation';
import Primus from 'primus';
import Scheduler from './scheduler';

/**
 * Main class used to define sensors, boards and their each properties
 * @event [started] when calls GardenerBase#run
 * @event [error] when any error occurs in GardenerBase#gardenerRobot
 * @event [stoped] when calls a GardenerBase#stop
 * @event [connect] when calls a GardenerBase#connect
 * @event [configured] when calls a GardenerBase#config
 * @event [reported] when call 'report' method
 * @event [scheduled] when scheduler emit 'ring' event
 * @required EventEmitter2
 * @required Scheduler
 * @required Primus
 * @required PanelModule
 * @required LightModule
 * @required TemperatureModule
 * @required HumidityModule
 * @required Lodash
 * @required Q
 * @class 
 */
export default class GardenerBase extends ee2.EventEmitter2 {
	/**
	 * GardenerBase constructor
	 * @param {Object} [panelConfig] panel boolean value
	 */
	constructor () {
		super({
			wildcard: true,
			delimiter: '::', 
			newListener: false, 
			maxListeners: 20
		});
		
		/**
  	 * Set of robot instance
  	 * @type {Robot}
  	 * @private
  	 */
		this._gardenerRobot;

		/**
	   * Set of debug mode
	   * @type {Boolean}
	   * @private
	   */
		this._debugMode;

		/**
	   * Set of modules object
	   * @type {Object}
	   * @private
	   */
		this._modules = {};

		/**
	   * Set of socket instance
	   * @type {Primus}
	   * @private
	   */
		this._socket;

		/**
	   * Set of socket connected to big brother
	   * @type {Boolean}
	   * @private
	   */
		this._isConnected = false;

		/**
	   * Set of is configured flag
	   * @type {Boolean}
	   * @private
	   */
		this._isConfigured = false;

		/**
	   * Set of is running flag
	   * @type {Boolean}
	   * @private
	   */
		this._isRunning = false;

		/**	
		 * Set of Scheduler instance
		 * @type {Scheduler}
		 * @private
		 */
		this._reportSchedule =  new Scheduler({
			timeFormat: DEFAULT_GARDENERBASE_PARAMS.REPORT_INTERVAL_TIME
		});
	}

	/**
	 * Add a module with respective specialty, for example: temperature, humidity and others.
	 * TODO: implements this method
	 * @param {Object} [config] hash has contains a robot config
	 * @return {Board} [gardener] robot with module added
	 * @type {function}
	 * @public
	 *
	 * Example:
	 * {
   *   "name": "example-module",
   *   "devices": {
   *     "example-sensor": {...},
   *     "example-relay": {...}
   *   }
   * }
	 */
	addModule (module, moduleKey) {
		if (lodash.has(this._modules, moduleKey)) {
			logger(`[Module] ${moduleKey} it already exists in gardener robot`);
		} else {
			logger(`[Module] ${moduleKey} has been added to gardener robot`);
		}
	}

	/**
	 * TODO: Remove a module.
	 * @param  {String} [moduleKey] module key
	 * @type {function}
	 * @public
	 */
	removeModule (moduleKey) {}

	/**
	 * Configure a gardener robot
	 * @event [configured] when calls a GardenerBase#config
	 * @param {Object} [config] hash has contains a config param of gardener robot
	 * @type {function}
	 * @public
	 *
	 * Example:
	 * { 
	 *	"name": "example-name",
	 * 	"debugMode": false, 	[optional]
	 *  "connections": {...},
	 *  "modules": {...}
	 * }
	 */
	config (config, run = false) {
		let gardenerRobotConfig;

		if (lodash.isUndefined(config)) {
			throw new Error('Missing config param in GardenerBase config method.');
		}

		// TODO: adds validation when connection if fail being that is current conection

		this._debugMode = config.debugMode || DEFAULT_GARDENERBASE_PARAMS.DEBUGMODE;

		gardenerRobotConfig = {
			name: config.name || GENERICS.STRING_NOT_FOUND,
			connections: {},
			devices: {}
		};

		lodash.assign(gardenerRobotConfig.connections, config.connections);

		// TODO: validate if config.modules keys has present in current gardener modules
		// TODO: adds lodash.includes method to config modules types

		// Adds device coording to the devices who want to add from `big brother API` config JSON file
		// TODO: adds repective test comparing `Robot.prototype.toJSON` method
		gardenerRobotConfig.devices = lodash.reduce(config.modules, (devices, module, moduleKey) => {
			lodash.assign(devices, lodash.reduce(module.devices, (d, device, deviceKey) => {
				d[moduleKey + capitalizeString(deviceKey)] = device;
				return d;
			}, devices));
			return devices;
		}, {});

		// TODO: Find how to use callback method in `robot` function
		this._gardenerRobot = Cylon.robot(gardenerRobotConfig);

		lodash.forEach(config.modules, (module, moduleKey) => {
			let newModule;
			
			switch (moduleKey) {
				case DEFAULT_MODULE_TYPES.PANEL:
					newModule = new PanelModule(module, this);
					break;

				case DEFAULT_MODULE_TYPES.LIGHT:
					newModule = new LightModule(module, this);
					break;

				case DEFAULT_MODULE_TYPES.TEMPERATURE:
					newModule = new TemperatureModule(module, this);
					break;

				case DEFAULT_MODULE_TYPES.HUMIDITY:
					newModule = new HumidityModule(module, this);
					break;

				case DEFAULT_MODULE_TYPES.IRRIGATION:
					newModule = new IrrigationModule(module, this);
					break;
			}

			this._modules[moduleKey] = newModule;
		});

		this._isConfigured = true;
		this.emit('configured');

		if (run) {
			this.run();
		}
	}

	/**
	 * Run the gardener robot
	 * @event [started] when calls GardenerBase#run
	 * @event [reported] when call 'report' method
	 * @event [error] when any error occurs in GardenerBase#gardenerRobot
	 * @param {Object} [config] hash has contains a config param of gardener
	 * @return {Object} socket instance
	 * @type {function}
	 * @public
	 */
	run () {
		let defer = q.defer();
		this._validates.runIsConfigured(this._isConfigured);
		this._validates.runIsRunning(this._isRunning);

		// TODO: Find how to use callback method in `stop` function
		this._gardenerRobot.start({testMode: this._debugMode});
		
		/**
		 * Listen to GardenerBase#gardenerRobot 'ready' event
		 */
		this._gardenerRobot.on('ready', data => {
			this._isRunning = true;
			this.emit('started');
			defer.resolve(data);
		});

		/**
		 * Listen to GardenerBase#gardenerRobot 'error' event
		 */
		this._gardenerRobot.on('error', err => {
			this.emit('error');
			defer.reject(err);
		});

		/**
		 * Listen to GardenerBase 'started' event
		 */
		this.on('started', () => {
			this._reportSchedule.on('ring', () => {
				this.report()
					.then(report => {
						this.emit('reported', report);
					});
			});
		})

		return defer.promise;
	}

	/**
	 * Stop the gardener robot
	 * @event [stoped] when robot is stop
	 * @type {function}
	 * @public
	 */
	stop () {
		// TODO: implements this validation
		// this._validates.runIsConfigured(this._isConfigured);
		this._validates.stop(this._isRunning);
		
		this._gardenerRobot.halt();
		this._isRunning = false;
		this.emit('stoped');
	}

	/**
	 * Method for connect to bigbrother server API
	 * @event [connected] call GardenerBase#connect
	 * @event [disconnected] when GardenerBase#socket is disconnect from big brother server
	 * @event [reconnected] when GardenerBase#socket is reconnected to big brother server
	 * @type {function}
	 * @public
	 */
	connect () {
		let url;

		this._validates.connectIsConnected(this._isConnected);

		url = `ws://${process.env.BIGBROTHER_URL}:${process.env.BIGBROTHER_PORT}`;
		// TODO: adds respective message for unconnect case
		this._socket = new Primus.createSocket()(url);
		this._isConnected = true;

		// TODO: emit this event when connection is ready
		this.emit('connected');

		gardenerBasePrimusListeners(this);
		gardenerBasePrimusEmitters(this);

		/**
		 * Listen to GardenerBase#socket 'close' event
		 * TODO: should also used: reconnect, reconnect timeout and destroy events
		 */
		this._socket.on('close', data => {
			this.emit('disconnected');
		});

		/**
		 * Listen to GardenerBase#socket 'reconnected' event
		 */
		this._socket.on('reconnected', data => {
			this.emit('reconnected');
		});
	}

	/**
	 * Method for disconnect to bigbrother server API
	 * @return {Promise} promise that contains
	 * @type {function}
	 * @public
	 */
	disconnect () {
		this._validates.disconnect(this._isConnected);	

		this._socket.end();
		this._isConnected = false;
	}

	/**	
	 * Method for pause/reanude report scheduler
	 * @event [scheduled] when call GardenerBase#toggleScheduler
	 * @return {Boolean} isScheduled boolean value
	 * @type {function}
	 * @public
	 */
	toggleScheduler () {
		let isScheduled;

		this._reportSchedule.toggle();
		isScheduled = this._reportSchedule.isScheduled;
		
		this.emit('scheduled', {isScheduled: isScheduled});

		return isScheduled;
	}

	/**
	 * Method that generate gardener base and robot report
	 * @return {Promise} gardener resolved information
	 * @type {function}
	 * @public
	 */
	report () {
		let defer, promises, reportValues;

		defer = q.defer();
		promises = [];
		reportValues = this.getInfo();
		reportValues.modules = {};

		lodash.forEach(this._modules, (module, moduleKey) => {
			if (module.showInReport) {
				promises.push(module.getInfo());
			}
		});

		// TODO:find way to reject in this promise
		q.all(promises)
			.then(modules => {
				let omit;

				lodash.forEach(modules, module => {
					reportValues.modules[module.name] = module;
				});

				defer.resolve(reportValues);
			});

		return defer.promise;
	}

	/**
	 * Method has return a gardener base and robot information
	 * @return {Object} gardener base and robot information
	 * @type {function}
	 * @public
	 */
	getInfo () {
		let baseInfo;

		baseInfo = {
			debugMode: this._debugMode,
			isConfigured: this._isConfigured,
			isRunning: this._isRunning,
			isConnected: this._isConnected
		}

		return baseInfo;
	}

	/**
	 * Method has return a gardener socket instance
	 * @return {Object} socket instance
	 * @type {function}
	 * @public
	 */
	get socket () {
		return this._socket;
	}

	/**
	 * Method has return a gardener modules object
	 * @return {Object} modules values
	 * @type {function}
	 * @public
	 */
	get modules () {
		return this._modules;
	}

	/**
	 * Method has return a gardener robot instance
	 * @return {Object} gardenerRobot value
	 * @type {function}
	 * @public
	 */
	get gardenerRobot () {
		return this._gardenerRobot;
	}

	/**
	 * Method has return a isConnected value var
	 * @return {Object} isConnected value
	 * @type {function}
	 * @public
	 */
	get isConnected () {
		return this._isConnected;
	}

	/**
	 * Method has return a isConfigured value var
	 * @return {Object} isConfigured value
	 * @type {function}
	 * @public
	 */
	get isConfigured () {
		return this._isConfigured;
	}

	/**
	 * Method has return a isRunning value var
	 * @return {Object} isRunning value
	 * @type {function}
	 * @public
	 */
	get isRunning () {
		return this._isRunning;
	}

	/**
	 * Return object with validates functions for GardenerBase Class
	 * @return {Object} all validation hash values
	 * @type {function}
	 * @private
	 */
	get _validates () {
		return {
			config: () => {},
			runIsConfigured: (isConfigured) => {
				if (!isConfigured) {throw new Error('The gardener is not configure')};
			},
			runIsRunning: (isRunning) => {
				if (isRunning) {throw new Error('The gardener is running')};
			},
			stop: (isRunning) => {
				if (!isRunning) {throw new Error('The gardener is already stoped')};
			},
			connectIsRunning: (isRunning) => {
				if (!isRunning) {throw new Error('The greenhouse is not running')};
			},
			connectIsConnected: (isConnected) => {
				if (isConnected) {throw new Error('The greenhouse is connected to big brother')};
			},
			disconnect: (isConnected) => {
				if (!isConnected) {throw new Error('The greenhouse is disconnect to big brother')};
			}
		}
	}
}
