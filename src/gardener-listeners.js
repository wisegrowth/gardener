/**
* @module Gardener Base Primus Listeners
* @author Camilo Acuña
*/

import logger from './logger';
import lodash from 'lodash';
import {getArduinoPort, messagesPacker} from './utils';

/**
* void function for define gardener primus listerners functions
* @param {GardenerBase} gardener instance of gardener
* @type {function}
* @public
*/
export default function gardenerBasePrimusListeners (gardener) {
  const ACTIONS = {
    CONFIG: 'config',
    GET_INFO: 'get-info',
    REPORT: 'report',
    RUN: 'run',
    STOP: 'stop',
    TOGGLE_SCHEDULER: 'toggle-scheduler'
  };

  /**
   * Listen to 'open' socket event and join in corresponding rooms
   */
  gardener.socket.on('open', () => {
    lodash.forEach(ACTIONS, action => {
      let msg = messagesPacker.join(`base:${action}`);
      gardener.socket.write(msg);
    });
  });

  /**
   * Listen to 'data' socket event and performs the appropriate action
   */
  gardener.socket.on('data', (message) => {
    /** 
     * If condition for listen only `request` actions
     */
    if (message.action === 'request') {
      /** 
       * If condition for `base:config` room
       */
      if (message.room === `base:${ACTIONS.CONFIG}`) {
        var run = false;
        
        if (lodash.isUndefined(message.data.connections.arduino.port)) {
          logger('Getting Arduino USB port...');
          getArduinoPort()
            .then(port => {
              gardener.on('configured', () => {
                let msg = messagesPacker.response(message.room);
                gardener.socket.write(msg);
              });

              gardener.on('started', () => {
                let msg = messagesPacker.response(message.room);
                gardener.socket.write(msg);
              });

              if (!lodash.isUndefined(message.data.run) && message.data.run) {
                run = true;
              }

              message.data.connections.arduino.port = port;
              gardener.config(message.data, run);
            }, err => {
              throw new Error('confict at get arduino port');
            });
        } else {
          if (!lodash.isUndefined(message.data.run) && message.data.run) {
            run = true;
          }

          gardener.config(message.data, run);
        }
      }
      /** 
       * If condition for `base:run` room
       */
      if (message.room === `base:${ACTIONS.RUN}`) {
        gardener.on('started', () => {
          let msg = messagesPacker.response(message.room);
          gardener.socket.write(msg);
        })

        gardener.run();
      }
      /** 
       * If condition for `base:stop` room
       */
      if (message.room === `base:${ACTIONS.STOP}`) {
        gardener.on('stoped', () => {
          let msg = messagesPacker.response(message.room);
          gardener.socket.write(msg);
        });

        gardener.stop();
      }
      /** 
       * If condition for `base:get-info` room
       */
      if (message.room === `base:${ACTIONS.GET_INFO}`) {
        let data, msg;

        data = gardener.getInfo();
        msg = messagesPacker.response(message.room, data);
        
        gardener.socket.write(msg);
      }
      /** 
       * If condition for `base:toggle-scheduler` room
       */
      if (message.room === `base:${ACTIONS.TOGGLE_SCHEDULER}`) {
        let data, msg;

        data = gardener.toggleScheduler();
        msg = messagesPacker.response(message.room, data);

        gardener.socket.write(msg);
      }
      /** 
       * If condition for `base:report` room
       */
      if (message.room === `base:${ACTIONS.REPORT}`) {
        gardener.report()
          .then(report => {
            let msg = messagesPacker.response(message.room, {report: report});
            gardener.socket.write(msg);
          })
      }
    }
  });
}
