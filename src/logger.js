/**
* @module Logger
* @author Camilo Acuña
*/ 

/**
* Helper method that show any log in develoment mode
* @param {String} msg text for show in log console
* @type {function}
* @public
*/
export default function logger (msg) {
  if (process.env.NODE_ENV === 'development') {
    console.log(msg);
  }
}
