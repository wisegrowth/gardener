
/**
* @module Gardener Base Primus Emitters
* @author Camilo Acuña
*/

import logger from './logger';
import lodash from 'lodash';
import {messagesPacker} from './utils';

/**
* Method to define gardener emitters functions
* @param {GardenerBase} gardener gardener instance
* @type {function}
* @public
*/
export default function gardenerBasePrimusEmitters (gardener) {
  const ACTIONS = {
    REPORTED: 'reported'
  };

  lodash.forEach(ACTIONS, action => {
    let msg = messagesPacker.join(`base:${action}`);
    gardener.socket.write(msg);
  });

  /**
   *  Garderner listener for 'started' event
   */
  gardener.on('started', () => {
    logger('[Status] Gardener robot has been started');
  });

  /**
   *  Garderner listener for 'error' event
   */
  gardener.on('error', () => {
    logger('[Status] Gardener robot has been error');
  });

  /**
   *  Garderner listener for 'stoped' event
   */
  gardener.on('stoped', () => {
    logger('[Status] Gardener robot has been stoped');
  });

  /**
   *  Garderner listener for 'connected' event
   */
  gardener.on('connected', () => {
    logger('[Connection] Gardener socket client ready');
  });

  /**
   *  Garderner listener for 'disconnected' event
   */
  gardener.on('disconnected', () => {
    logger('[Connection] Gardener socket client is disconnect');
  });

  /**
   *  Garderner listener for 'configured' event
   */
  gardener.on('configured', () => {
    logger('[Configuration] Gardener robot has been configured');
  });

  /**
   * Gardener listener for 'scheduled' event
   */
   gardener.on('scheduled', data => {
    let state = data.isScheduled ? 'on' : 'off';
    logger(`[Scheduler] Gardener report scheduler is ${state}`);
   });

   /**
    * Gardener listener for 'reported' event
    */
   gardener.on('reported', report => {
    let msg = messagesPacker.stream(`base:${ACTIONS.REPORTED}`, report);
    gardener.socket.write(msg);
   });
}
